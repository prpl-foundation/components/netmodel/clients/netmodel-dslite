# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]


## Release v0.1.3 - 2022-11-28(15:39:33 +0000)

### Other

- [NetModel] Update copyright information

## Release v0.1.2 - 2022-11-22(13:07:51 +0000)

### Other

- Opensource component

## Release v0.1.1 - 2022-11-21(16:22:11 +0000)

### Fixes

- Sync Name parameter in DSLite NetModel client

## Release v0.1.0 - 2022-11-17(07:09:54 +0000)

### New

- [dslite][netmodel] Create a netmodel client for dslite interfaces.

## Release 0.0.0 - 2022-11-16(13:10:20 +0000)


